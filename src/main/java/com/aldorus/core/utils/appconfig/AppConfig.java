package com.aldorus.core.utils.appconfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class AppConfig {
    private static Map<String, String> properties;

    private AppConfig() {

    }

    public static void init(String filePath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        init(fileInputStream);
    }

    public static void init(FileInputStream inputStream) throws IOException {
        if (properties == null) {
            Properties prop = new Properties();
            prop.load(inputStream);
            properties = new HashMap<>();
            prop.forEach((key, value) -> properties.put(key.toString(), value.toString()));
            prop = null;
        }
    }



    public static String get(String key) {
        return properties.get(key);
    }

    public static <T> T get(String key, Class<T> tClass) {
        return (T)properties.get(key);
    }

}
